﻿namespace WcfServiceDemo1
{
    public class Employee
    {
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
    }
}