﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfServiceDemo1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : IService1
    {
        public string InsertUser(Employee employee)
        {
            string message=" ";

            SqlConnection con = new SqlConnection("Data Source=HP-PROBOOK-4440;Initial Catalog=Registration;Integrated Security=True");

            con.Open();
            SqlCommand command = new SqlCommand("usp_AddRegistrationTable", con);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            
            command.Parameters.AddWithValue("@UserName", employee.UserName);
            command.Parameters.AddWithValue("@Password", employee.Password);
            command.Parameters.AddWithValue("@Country", employee.Country);
            command.Parameters.AddWithValue("@Email", employee.Email);
            int result = command.ExecuteNonQuery();
            if(result==1)
            {
                message = employee.UserName + "insserted record succcesfully";
                }
            else
            {
                message = employee.UserName + "record not inserted";
            }
                con.Close();
            return message;
        }


        //***********************************//

    
        public string UpdateUser(Employee employee)

        {
            string messge = "updated record successfully";
            SqlConnection con = new SqlConnection("Data Source=HP-PROBOOK-4440;Initial Catalog=Registration;Integrated Security=True");
            con.Open();
            SqlCommand command = new SqlCommand("usp_UpdateRegistrationTable1", con);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID", employee.UserID);
            command.Parameters.AddWithValue("@UserName", employee.UserName);
            command.Parameters.AddWithValue("@Password", employee.Password);
            command.Parameters.AddWithValue("@Country", employee.Country);
            command.Parameters.AddWithValue("@Email", employee.Email);
            command.ExecuteNonQuery();
            con.Close();
            return messge;
        }




        // ***************************//


        public   string deleteUser(string  id)
        {

            string message = "deleted record succcessfully";
            SqlConnection con =new SqlConnection("Data Source=HP-PROBOOK-4440;Initial Catalog=Registration;Integrated Security=True");
             con.Open();
            SqlCommand command = new SqlCommand("usp_deleteRegistrationTable", con);
            command.CommandType = System.Data.CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserID",id);
            command.ExecuteNonQuery();
            con.Close();
             return message;

        }

    }
    }

